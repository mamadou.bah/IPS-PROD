#include "basis.h"
#include "math.h"
#include "Poly.h"

Basis::Basis(){}

Basis::Basis(double br_,double bz_, int N, double Q){
  int i=0,j;
  br=br_;bz=bz_;
  double res, k= float(2)/3,A=((N+2)*pow(Q,k)+0.5)/Q,Z=A-1/Q;
  mMax=int(Z);
  nMax=arma::vec(mMax);
  for (i=0;i<mMax;i++){
    nMax(i)=int(0.5*(mMax-i-1)+1);
  }
  n_zMax=arma::mat(mMax,nMax(0),arma::fill::zeros);
  for(i=0;i<mMax;i++){
    j=0;
    while(j<nMax(i)){
      res=(N+2)*pow(Q,k)+0.5 -(i+2*j+1)*Q;
      n_zMax(i,j)=int(res);
      j++;
    }
  }

}

long Basis::factorielle(int n){
  if(n<=0){return 1;}

  else{
    return (n*factorielle(n-1));
  }

}

arma::vec Basis::rPart(arma::vec r , int m, int n){
  double a1,a2,a3,a4,b1,b2,b3, PI  =3.141592653589793238463;
  Poly L1;int i,nz;
  arma::vec r2= (r%r)/(pow(br,2));
  L1.calcLaguerre(m+1,n+1,r2);
  arma::vec L2= L1.laguerre(m,n);
  arma::vec res=arma::vec(r.n_elem,arma::fill::zeros);
  a1=double(1)/(br*sqrt(PI));
  b1=double(factorielle(n))/factorielle(n+m);
  a2=sqrt(b1);
  b3=2*pow(br,2); 
  for(i=0;i<r.n_elem;i++){
    b2= pow(r(i),2);
    a3=exp(-b2/b3);
    a4=pow(r(i)/br,m);
    res(i)=a1*a2*a3*a4*L2(i);
  }

  return res;
}

arma::vec Basis::zPart(arma::vec z ,int nz){
  double a1,a2,a3,b1,b2,b3,b4,PI=3.141592653589793238463;
  int i;
  Poly H1;
  arma::vec res=arma::vec(z.n_elem,arma::fill::zeros);
  arma::vec z2= z/bz;
  H1.calcHermite(nz+1,z2);
  arma::vec H2= H1.hermite(nz);
  a1=sqrt(bz);
  b1=pow(2,nz);
  b2=sqrt(PI)*factorielle(nz);
  a2=sqrt(b1*b2);
  for(i=0;i<z.n_elem;i++){   
    b3=-pow(z(i),2);
    b4=2*pow(bz,2);
    a3=exp(b3/b4);
    res(i)=(double(1)/(a1*a2))*a3*H2(i);
  }
  return res;			  
}

arma::mat Basis::basisFunc(int m,int n,int n_z,arma::vec zVals,arma::vec rVals){

  return (rPart(rVals,m,n)*zPart(zVals,n_z).t());  ;



} 

