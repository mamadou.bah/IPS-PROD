#include "Poly.h"

void Poly::calcHermite(int n,arma::vec Z){
  H_elem=Z.n_elem;
  H = arma::mat(Z.n_elem,1,arma::fill::ones);
  int i;
  H.insert_cols(1,2*Z);
  for(i=2; i<n; i++){

        arma::vec A = 2*Z%H.col(i-1)-(2*(i-1)*H.col(i-2));
        H.insert_cols(i, A);
  }

}

void Poly::calcLaguerre(int m,int n, arma::vec Z){

  L_elem=Z.n_elem;
  int len=Z.n_elem,i,j,k;
  L = arma::cube(len,m,1,arma::fill::ones);
  arma::cube S= arma::cube(len,m,1,arma::fill::zeros);
  for(i=0;i<m;i++){
    for(j=0;j<len;j++){
      S(j,i,0)=1+i-Z(j);
    }
   
  }
  L.insert_slices(1,S);
  L.resize(len,m,n);
  for(i=2;i<n;i++){
    for(j=0;j<m;j++){
      double k1=(2+double(j-1)/i); 
      double k2=(1+double(j-1)/i);
      for(k=0;k<len;k++){
        L(k,j,i)=(k1-Z(k)/i)*L(k,j,i-1)-k2*L(k,j,i-2);
      }
    }
  }


}

arma::vec Poly::hermite(int n){
  arma::vec A= arma::vec(H_elem,arma::fill::randn);
  int i;
  for (i=0; i<H_elem; i++){
      A(i)=H(i,n);
  }
  return A;

}

arma::vec Poly::laguerre(int m, int n){
  arma::vec A= arma::vec(L_elem,arma::fill::zeros);
  int i;
  for (i=0; i<L_elem; i++){
        A(i)=L(i,m,n);
        }
  return A;
}
