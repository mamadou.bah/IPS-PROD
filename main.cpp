#include "Poly.h"
#include "basis.h"
#include <fstream>
#include <iostream>
#include <ctime>
using namespace std;

int main()
{
  Poly poly;
  arma::mat rho;
  rho.load("rho.arma", arma::arma_ascii);
  arma::vec zVals = {-10.1, -8.4, -1.0, 0.0, 0.1, 4.3, 9.2, 13.7};
  Basis basis(1.935801664793151,      2.829683956491218,     14,     1.3);
  arma::vec rVals = {3.1, 2.3, 1.0, 0.0, 0.1, 4.3, 9.2, 13.7};
  int nbR = rVals.n_elem;
  int nbZ = zVals.n_elem;	

  arma::mat result = arma::zeros(nbR, nbZ); // number of points on r- and z- axes
  clock_t start;
  double duree;
  uint i = 0;
  uint j = 0;
  start= clock();
  for (int m = 0; m < basis.mMax; m++)
    {
      for (int n = 0; n < basis.nMax(m); n++)
	{
	  for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
	    {
	      j=0;
	      for (int mp = 0; mp < basis.mMax; mp++)
		{
		  for (int np = 0; np < basis.nMax(mp); np++)
		    {
		      for (int n_zp = 0; n_zp < basis.n_zMax(mp, np); n_zp++)
			{
			  arma::mat funcA = basis.basisFunc( m,  n,  n_z, zVals, rVals);
			  arma::mat funcB = basis.basisFunc(mp, np, n_zp, zVals, rVals);
			  result += funcA % funcB * rho(i,j); // mat += mat % mat * double
			  j++;
			}
		    }
		}
	      i++;
	    }
	}
    }
  duree = ( std::clock() - start ) / (double) CLOCKS_PER_SEC ;
  printf("t1 vaut %f \n",duree);
  

  uint len = 0;
  arma::cube Calc;
  result = arma::zeros(nbR, nbZ);
  
  for (int m = 0; m < basis.mMax; m++) 
      {
	  for (int n = 0; n < basis.nMax(m); n++)
	    {
	      len+=basis.n_zMax(m,n);
	    }
      }
    
    Calc= arma ::cube(nbR,nbZ,len,arma::fill::zeros);
    i=0;
	
    for (int m = 0; m < basis.mMax; m++)
      {
	for (int n = 0; n < basis.nMax(m); n++)
	  {
	    for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
	      {
		Calc.slice(i)=basis.basisFunc(m, n, n_z, zVals, rVals);
		i++;
	      }
	  }
      }	
	


    for (i = 0; i < len; i++)
      {
	arma::mat result2 = arma::zeros(nbR, nbZ);
	
	for (j = 0; j < len; j++)
	  {					
	    result2 += Calc.slice(j) * rho(i,j);
	  }
	result += Calc.slice(i) % result2; 
      }
    
    result.print("resultat");
    double duree2=duree;
    duree2 = ( std::clock() - duree ) / (double) CLOCKS_PER_SEC ;
    printf("t2 vaut %f \n",duree2-duree);
    ofstream fichier1("resultat.txt", ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
        
    if(fichier1) 
        {     		     
	  fichier1<<result;
          fichier1.close();	 	       
        }
        else 
	  {
	    cerr << "Erreur à l'ouverture !" << endl;}
 

  return 0;
}
