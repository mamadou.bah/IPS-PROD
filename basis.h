#ifndef BASIS_H
#define BASIS_H
#include <armadillo>

class Basis
{
 public:
  int mMax;
  arma::vec nMax;
  arma::mat n_zMax;
  double br,bz;
  Basis(double br, double bz, int N, double Q);
  Basis();
  long factorielle(int n);
  arma::mat basisFunc(int m,int n,int n_z,arma::vec zVals,arma::vec rVals);
  arma::vec rPart(arma::vec r,int m ,int n);
  arma::vec zPart(arma::vec z,int n);
}
;

#endif
