#ifndef POLY_H
#define POLY_H
#include <armadillo>
class Poly
{

  
  public:
  arma::mat H;
  arma::cube L;
  void calcHermite(int n, arma::vec Z);
  void calcLaguerre(int l,int m, arma::vec Z);
  arma::vec hermite(int n);
  arma::vec laguerre(int l,int m);
  int H_elem;
  int L_elem;
};

#endif
