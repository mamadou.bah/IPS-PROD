main: main.o Poly.o basis.o
	g++ -std=c++11 -o main main.o Poly.o basis.o -larmadillo 
	
main.o: main.cpp 
	g++ -c main.cpp 

Poly.o: Poly.cpp Poly.h
	g++ -c Poly.cpp 

basis.o: basis.cpp basis.h
	g++ -c basis.cpp -lm

tests: tests.o Poly.o
	g++ tests.o Poly.o -o tests -larmadillo

tests.o: tests.cpp 
	g++ -Wall -c tests.cpp -o tests.o

tests.cpp: MyTestSuite.h
	cxxtestgen --error-printer -o tests.cpp MyTestSuite.h

testbasis: testbasis.o basis.o Poly.o
	g++ testbasis.o basis.o Poly.o -o testbasis -larmadillo

testbasis.o: testbasis.cpp 
	g++ -Wall -c testbasis.cpp -o testbasis.o

testbasis.cpp: TestBasis.h
	cxxtestgen --error-printer -o testbasis.cpp TestBasis.h

testbasisR: testbasisR.o basis.o Poly.o
	g++ testbasisR.o basis.o Poly.o -o testbasisR -larmadillo

testbasisR.o: testbasisR.cpp 
	g++ -Wall -c testbasisR.cpp -o testbasisR.o

testbasisR.cpp: TestBasisR.h
	cxxtestgen --error-printer -o testbasisR.cpp TestBasisR.h

testbasisZ: testbasisZ.o basis.o Poly.o
	g++ testbasisZ.o basis.o Poly.o -o testbasisZ -larmadillo

testbasisZ.o: testbasisZ.cpp 
	g++ -Wall -c testbasisZ.cpp -o testbasisZ.o

testbasisZ.cpp: TestBasisZ.h
	cxxtestgen --error-printer -o testbasisZ.cpp TestBasisZ.h




